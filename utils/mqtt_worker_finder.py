import signal
import time
import json
from json import JSONDecodeError

import workernode.models as models
from utils import mqtt_interface


__all__ = ["find_new_workers"]


class TaskKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, *args):
        self.kill_now = True


def create_new_worker(ip_address):
    if existing_node := models.WorkerNode.objects.filter(ip_address=ip_address):
        if not existing_node[0].reachable:
            print("Making an existing node reachable!")
            existing_node[0].set_reachable()
            if existing_node[0].task is not None:
                existing_node[0].free()
    else:
        print("Creating a new node!")
        models.WorkerNode.objects.create(ip_address=ip_address)


def on_message_new_worker(client, userdata, message):
    print(f"received message: {str(message.payload.decode('utf-8'))}")
    try:
        create_new_worker(
            json.loads(str(message.payload.decode("utf-8"))).get("ip_address")
        )
    except JSONDecodeError:
        print("Invalid message format!")


def find_new_workers():
    killer = TaskKiller()
    client = mqtt_interface.connect_client(
        "raspberry_devices",
        "devices",
        on_message_new_worker
    )

    while not killer.kill_now:
        time.sleep(3600)

    mqtt_interface.disconnect_client(client)
