import json
import time
import threading
from collections import defaultdict
import operator

import channels.layers
from django.conf import settings
from asgiref.sync import async_to_sync
from workernode.models import WorkerNode

from . import mqtt_interface
import config


def normalize_ops(ops):
    from math import ceil
    return config.OPS_UNIT * ceil(ops / config.OPS_UNIT)


class LoadBalancer:
    def __init__(self):
        # execution's helper variables
        self.LOCK = threading.Lock()
        self.OPS_COUNTER_DICT = defaultdict(int)
        self.CHANNEL = channels.layers.get_channel_layer()
        # execution's flag variables
        self.available = True
        self.stable_exec = False
        self.fairness = False
        # workers' variables
        self.leader = None
        self.task = None
        self.working_nodes = []

    @classmethod
    def get_working_nodes(cls):
        """This function is just an interface to the WorkerNode class
        method to retrieve the nodes that are currently busy.
        """
        return [node for node in WorkerNode.get_busy_nodes()]

    def get_workers_ops(self):
        return {node.pk: node.current_ops for node in self.working_nodes}

    def get_exec_data(self):
        """This function retrieves the execution metadata like the
        current number of operations per second achieved and the
        current number of operations per second made by each worker.
        A LOCK is required because the OPS_COUNTER_DICT is reached
        also by the thread which receives MQTT messages from workers
        (the thread calls the on_message_callback function).
        """
        self.LOCK.acquire()
        curr_ops = sum(self.OPS_COUNTER_DICT.values())
        json_workers_ops = json.dumps(self.OPS_COUNTER_DICT)
        for k in self.OPS_COUNTER_DICT:
            # after each cycle, the counter must be reinitialized
            self.OPS_COUNTER_DICT[k] = 0
        self.LOCK.release()
        return curr_ops, json_workers_ops

    def set_leader_node(self, worker_node_pk=None):
        """This function just sets the leader node of the execution."""
        self.leader = None
        if worker_node_pk is not None:
            self.leader = WorkerNode.objects.get(pk=worker_node_pk)

    def set_exec_helpers(self):
        """This function sets the data structures which are used to
        make the load balancer work as expected.
        """
        self.working_nodes = [self.leader]
        self.task = self.leader.task

    def reset_variables(self):
        self.OPS_COUNTER_DICT = defaultdict(int)
        self.available = True
        self.stable_exec = False
        self.fairness = False
        self.leader = None
        self.task = None
        self.working_nodes = []

    def on_message_callback(self, client, userdata, message):
        """This function is called from the "MQTT thread" each time it
        receives a new message from the workers. It just updates the
        counter of each worker node.
        """
        self.LOCK.acquire()
        self.OPS_COUNTER_DICT[
            json.loads(str(message.payload.decode("utf-8"))).get("ip_address")
        ] += 1
        self.LOCK.release()

    def send_to_channel(self, data):
        """This function sends the given data through the django
        channels interface. The aim is to display live data through a
        web socket.
        """
        async_to_sync(self.CHANNEL.group_send)(
            settings.LIVE_CHART_CHANNEL_NAME, {
                "type": "data_received",
                "data": json.dumps(data),
            }
        )

    def send_exec_data(self, curr_ops, json_workers_ops):
        """This function just creates the django channels' payload."""
        data = {
            "timestamp": time.time(),
            "workers": len(self.working_nodes),
            "target_ops": self.task.ops,
            "total_ops": curr_ops,
            "workers_ops": json_workers_ops,
            "available": self.available,
            "exception": False
        }
        self.send_to_channel(data)

    def clean_execution(self, mqtt_client, disaster=False):
        """This function resets the execution environment."""
        print("Clean phase")
        mqtt_interface.disconnect_client(mqtt_client)
        for node in self.working_nodes:
            if disaster:
                node.free(available=False)
            else:
                node.free()
        self.reset_variables()

    def update_workers_ops(self, current_ops):
        """This function is called each time the workers did not reach
        the ops requested by the user. If a new node was started with
        a number of ops that will fill that gap, in order to keep the
        number of operations per second closer to the user request as
        much as possible, the other nodes must reduce their operations
        per second ratio.
        """
        real_ops = normalize_ops(
            current_ops // len(self.working_nodes)
        )
        for worker in self.working_nodes:
            worker.async_update_ops(real_ops)

    def start_leader_node(self, mqtt_client):
        """This function starts the execution on the leader node.
        If the leader is unreachable the function will call the
        <disaster recovery> routine. If the launch become bad (no
        nodes available), the function will return False and the
        execution will stop.
        """
        try:
            self.leader.start_physical_twin()
            return True
        except Exception as ex:
            print(ex)
            self.send_to_channel(
                {"exception": True, "ex_message": str(ex), "helper": False}
            )
            self.disaster_recovery()
            if self.leader is None:
                self.clean_execution(mqtt_client, disaster=True)
                return False
            return True

    def disaster_recovery(self):
        """This function tries to recover an execution if the current
        leader node is not reachable, it achieves this with an election
        of a new leader node (if available).
        """
        if available_nodes := WorkerNode.get_available_nodes():
            try:
                # if the new node is reachable, it becomes the new leader
                available_nodes[0].sync_launch(self.task)
                self.set_leader_node(available_nodes[0].pk)
                self.set_exec_helpers()
                print(f"Now the leader is {self.leader}")
            except Exception as ex:
                print(ex)
                self.send_to_channel(
                    {"exception": True, "ex_message": str(ex), "helper": False}
                )
                # try to start the execution again with a different node
                self.disaster_recovery()
        else:
            self.send_to_channel({
                "available": False,
                "exception": True,
                "ex_message": str("All the nodes are down, try later."),
                "helper": False
            })
            self.set_leader_node()  # now the leader is None

    def increase_ops(self, curr_ops):
        """This function checks if it's possible to start the current
        execution on an additional node, otherwise it notifies that all
        the nodes are busy.
        """
        self.stable_exec, self.fairness = False, False
        if available_nodes := WorkerNode.get_available_nodes():
            # send the ops requested to the new node asynchronously
            available_nodes[0].async_help_request(self.task, curr_ops, self.task.ops)
            # update the workers' ops with the real ones
            self.update_workers_ops(curr_ops)
        else:
            self.available = False
            self.fairness = self.apply_fairness(recovery=True)

    def apply_fairness(self, recovery=False):
        """This function updates the ops requested by each worker in
        order to reach fairness. If all the workers are executing the
        same number of ops, the execution will become fair.
        """
        fair_ops = self.task.ops // len(self.working_nodes)
        fair_flag = True

        for worker in self.working_nodes:
            if fair_ops < worker.current_ops <= self.task.ops:
                worker.sync_update_ops(worker.current_ops, operator.sub)
                fair_flag = False
            elif 0 <= worker.current_ops < fair_ops:
                worker.sync_update_ops(worker.current_ops, operator.add)
                fair_flag = False
        return fair_flag if not recovery else False

    def balance_exec(self, worker_node_pk, duration):
        """This function is the core logic of the LoadBalancer class.
        Its goal is to achieve the execution requested by the user in a
        fair way (possibly).
        """
        self.set_leader_node(worker_node_pk)
        self.set_exec_helpers()
        mqtt_client = mqtt_interface.connect_client(
            "raspberry_results", "results", self.on_message_callback
        )
        duration = time.time() + int(duration)

        exec_status = self.start_leader_node(mqtt_client)
        while exec_status:
            if time.time() > duration:
                self.clean_execution(mqtt_client)
                break

            self.working_nodes = LoadBalancer.get_working_nodes()
            print(f"{self.working_nodes}")
            # wait a second each iteration to retrieve the right ops from MQTT client
            time.sleep(1)
            curr_ops, json_workers_ops = self.get_exec_data()
            self.send_exec_data(curr_ops, json_workers_ops)
            print(f"ops detected: {curr_ops}, ops requested: {self.task.ops}")
            if curr_ops < self.task.ops:
                print("Target ops not reached")
                # the LB is conservative, it launches only one new node at a time
                self.increase_ops(curr_ops)
            elif not self.stable_exec and len(self.working_nodes) > 1:
                # the target ops are reached and the load balancer can
                # balance the work load in the next iteration
                print("Now the execution will become stable!")
                self.stable_exec = True
            elif self.stable_exec and not self.fairness:
                print("Stable execution, applying fairness")
                self.fairness = self.apply_fairness()

    def start_async_conn(self, worker_node_pk, update, **kwargs):
        """This function starts a new connection to an available node
        asynchronously.
        """
        print("ASYNC CONNECTION")
        node = WorkerNode.objects.get(pk=worker_node_pk)

        if not node.reachable:
            return

        if not update:
            ops_needed = normalize_ops(
                abs(kwargs["ops_requested"] - kwargs["current_ops"])
            )
        elif kwargs["new_ops"] != node.current_ops:
            ops_needed = kwargs["new_ops"]
        else:
            # the op/s ratio is already updated: duplicate request
            return

        try:
            node.sync_help_request(ops_needed=ops_needed)
        except Exception as ex:
            print(ex)
            self.send_to_channel(
                {"exception": True, "ex_message": str(ex), "helper": True}
            )
        else:
            print(f"Requested to {node.ip_address}: {ops_needed} ops")
