import paho.mqtt.client as mqtt

from config import MQTT_BROKER

__all__ = ["connect_client", "disconnect_client"]


def connect_client(device_id, mqtt_topic, on_message_method):
    client = mqtt.Client(device_id)
    client.username_pw_set("raspberry", password="raspberryPwd")
    client.connect(MQTT_BROKER)
    client.loop_start()
    client.subscribe(mqtt_topic)
    client.on_message = on_message_method
    
    return client


def disconnect_client(client):
    client.loop_stop()
    client.disconnect()
