import time
import decimal
import psutil


def main():
    old_value = 0

    while True:
        net_counter = psutil.net_io_counters()
        new_value = net_counter.bytes_sent + net_counter.bytes_recv

        if old_value:
            send_stat(new_value - old_value)

        old_value = new_value
        time.sleep(1)


def convert_to_gbit(value):
    return decimal.Decimal(value / 1024 / 1024 / 1024 * 8)


def send_stat(value):
    print(convert_to_gbit(value))


if __name__ == "__main__":
    decimal.getcontext().prec = 6
    main()
