import urllib3
import json

from django.db import models
import requests
import celery

import config


class Task(models.Model):
    TASK_1 = 1
    TASK_2 = 2
    TASK_3 = 3
    TASK_CHOICE = [
        (TASK_1, "1 - Find prime numbers in interval"),
        (TASK_2, "2 - Calculate a pow of each millisecond"),
        (TASK_3, "3 - Calculate the factorial of a number"),
    ]
    DEFAULT_OPS = 100
    DEFAULT_INTERVAL = 999
    DEFAULT_EXPONENT = 3
    DEFAULT_FACTORIAL = 30

    number = models.PositiveSmallIntegerField(choices=TASK_CHOICE, default=TASK_1)
    # this value will be updated each time through a form:
    ops = models.PositiveSmallIntegerField(default=DEFAULT_OPS)
    interval_value = models.PositiveSmallIntegerField(default=DEFAULT_INTERVAL)
    pow_exponent = models.PositiveSmallIntegerField(default=DEFAULT_EXPONENT)
    fact_number = models.PositiveSmallIntegerField(default=DEFAULT_FACTORIAL)

    def __str__(self):
        return f"number: {self.number}, ops: {self.ops}"

    @property
    def get_configuration(self):
        return {
            "task": json.dumps({
                "task_number": self.number,
                "custom_value": self.get_custom_value,
                "ops_value": self.ops,
            })
        }

    @property
    def get_free_configuration(self):
        return {"task": "Stop!"}

    @property
    def get_custom_value(self):
        if self.number == 1:
            return self.interval_value
        elif self.number == 2:
            return self.pow_exponent
        elif self.number == 3:
            return self.fact_number

    def get_help_configuration(self, ops):
        return {
            "task": json.dumps({
                "task_number": self.number,
                "custom_value": self.get_custom_value,
                "ops_value": ops,
            })
        }


class WorkerNode(models.Model):
    ip_address = models.CharField(max_length=15)
    available = models.BooleanField(default=True)
    reachable = models.BooleanField(default=True)
    current_ops = models.PositiveSmallIntegerField(default=0)
    task = models.ForeignKey(
        Task,
        on_delete=models.SET_NULL,
        related_name="task",
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"ip: {self.ip_address}, av: {self.available}, rc: {self.reachable}"

    @classmethod
    def get_available_nodes(cls):
        return cls.objects.filter(available=True, reachable=True).all()

    @classmethod
    def get_busy_nodes(cls):
        return cls.objects.filter(available=False, reachable=True).all()

    @staticmethod
    def __send_json_http_request(url, json_data):
        headers = {"content-type": "application/json", }
        try:
            r = requests.put(
                url,
                json=json_data,
                headers=headers,
                timeout=config.REQUEST_TIMEOUT
            )
        except requests.exceptions.ConnectTimeout:
            return False
        except urllib3.exceptions.HTTPError:
            return False

        return True if r.status_code == 200 else False

    def bind(self, task):
        """Bind the task object to the working node."""
        self.task = task
        self.save()

    def set_busy(self):
        """Set the node not available for other executions."""
        self.available = False
        self.save()

    def set_unreachable(self):
        """The node is disconnected, set it unreachable."""
        self.reachable = False
        self.save()

    def set_reachable(self):
        """The node is reconnected, set it reachable."""
        self.reachable = True
        self.save()

    def set_current_ops(self, ops):
        """Update the op/s that the node is executing."""
        self.current_ops = ops
        self.save()

    def start_physical_twin(self, help_request=False, **kwargs):
        """This function sends an HTTP request to the physical node in
        order to start the execution requested by the user. A node can
        be the leader of the execution or a leader helper (in this case
        it must process a different value of op/s).
        """
        url = f"http://{self.ip_address}/things/task-execution/properties/task"

        if not help_request:
            json_data = self.task.get_configuration
            ops = self.task.ops
        else:
            json_data = self.task.get_help_configuration(kwargs["ops"])
            ops = kwargs["ops"]

        status = self.__send_json_http_request(
            url=url,
            json_data=json_data
        )
        if not status:
            self.set_unreachable()
            self.free(available=False)
            raise Exception(f"Can't connect to worker {self.ip_address}. Timeout.")

        self.set_current_ops(ops)
        self.set_busy()

    def sync_help_request(self, ops_needed):
        """Once the load balancer ask help to a new node, this function
        bind the task to the new helper and the starts it physically.
        """
        self.start_physical_twin(help_request=True, ops=ops_needed)

    def sync_launch(self, task):
        """This function only applies in case the current execution
        leader became dead, a new node must be started synchronously.
        """
        self.bind(task)
        self.start_physical_twin()

    def sync_update_ops(self, current_ops, operand):
        """This function is just an interface to the asynchronous
        update of the op/s that a node has to produce.
        """
        self.async_update_ops(operand(current_ops, config.OPS_UNIT))

    def async_launch(self, task):
        """This function only starts the async celery task."""
        self.bind(task)
        celery.current_app.send_task(
            "workernode.tasks.execute_task",
            args=(self.pk, config.TASK_DURATION, )
        )

    def async_help_request(self, task, current_ops, ops_requested):
        """Once the load balancer ask help to a new node, this function
        bind the task to the new helper and the starts it physically.
        In an asynchronous way.
        """
        self.bind(task)
        celery.current_app.send_task(
            "workernode.tasks.start_worker",
            args=(self.pk, ),
            kwargs={"current_ops": current_ops, "ops_requested": ops_requested}
        )

    def async_update_ops(self, new_ops):
        """Interface that hide the same call to help_request but with a
        different number of operations per second.
        """
        celery.current_app.send_task(
            "workernode.tasks.start_worker",
            args=(self.pk, True, ),
            kwargs={"new_ops": new_ops}
        )

    def free(self, available=True):
        """This function stops the execution on the physical node."""
        if available:
            status = self.__send_json_http_request(
                f"http://{self.ip_address}/things/task-execution/properties/task",
                self.task.get_free_configuration
            )
            if not status:
                self.set_unreachable()
                # this Exception can be logged
                print(f"Free - Can't connect to worker: {self.ip_address}. Timeout.")

        self.set_current_ops(0)
        self.task = None
        self.available = True
        self.save()


class Execution(models.Model):
    json_data = models.TextField()
    task_number = models.PositiveSmallIntegerField(default=0)
    task_ops = models.PositiveSmallIntegerField(default=0)
    workers_num = models.PositiveSmallIntegerField(default=0)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"Execution of Task[{self.task_number}] on {self.date}"
