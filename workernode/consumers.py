from channels.consumer import SyncConsumer
from django.conf import settings
from asgiref.sync import async_to_sync


class ChartConsumer(SyncConsumer):
    def websocket_connect(self, event):
        async_to_sync(self.channel_layer.group_add)(
            settings.LIVE_CHART_CHANNEL_NAME,
            self.channel_name
        )
        self.send({
            "type": "websocket.accept",
        })

    def websocket_disconnect(self, event):
        # Leave monitoring group
        async_to_sync(self.channel_layer.group_discard)(
            settings.LIVE_CHART_CHANNEL_NAME,
            self.channel_name
        )

    # Receive data from publisher script
    def data_received(self, event):
        # Send message to WebSocket
        self.send({
            "type": "websocket.send",
            "text": event["data"],
        })
