from django import forms
from django.core.exceptions import ValidationError

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit

from workernode.models import Task, WorkerNode


class TaskForm(forms.ModelForm):
    """Launch task for worker nodes."""

    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column("number", css_class="form-group"),
                css_class="form-row",
            ),
            Row(
                Column("interval_value", css_class="form-group task-1 d-none"),
                Column("pow_exponent", css_class="form-group task-2 d-none"),
                Column("fact_number", css_class="form-group task-3 d-none"),
                css_class="form-row",
            ),
            Row(
                Column("ops", css_class="form-group mb-0"),
                Column(
                    Submit("submit", "Launch", css_class="btn site-btn mb-3 w-75 font-5"),
                    css_class="d-flex align-items-end justify-content-end"
                ),
                css_class="form-row",
            ),
        )

    def clean(self):
        if self.cleaned_data["interval_value"] > self.instance.DEFAULT_INTERVAL:
            raise ValidationError(
                "Ensure the [ interval ] is less than or equal to 999.",
                code="invalid"
            )

        return super().clean()

    class Meta:
        model = Task
        fields = (
            "number",
            "ops",
            "interval_value",
            "pow_exponent",
            "fact_number",
        )
        labels = {
            "number": "Task type",
            "ops": "Operation per seconds",
            "interval_value": "Search interval for prime numbers (max: 999)",
            "pow_exponent": "Exponent of the pow",
            "fact_number": "Starting number",
        }
