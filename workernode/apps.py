from django.apps import AppConfig


class WorkernodeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'workernode'
