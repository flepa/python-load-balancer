from django.template.defaulttags import register


@register.filter
def get_task_from_int(task_idx):
    """
    Custom template filter that returns a task given an index.

    Args:
        task_idx (int): index of the task to retrieve.

    Returns:
        string: Related day.
    """
    tasks = {
        1: "1 - Find prime numbers in interval",
        2: "2 - Calculate a pow of each millisecond",
        3: "3 - Calculate the factorial of a number",
    }
    return tasks.get(task_idx)


@register.filter
def get_exception_from_int(ex_idx):
    ex = {
        1: "No Worker available!",
        2: "Celery Error!",
    }
    return ex.get(ex_idx)
