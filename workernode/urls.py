from django.urls import path
from workernode import views

app_name = "workernode"

urlpatterns = [
    path("<int:pk>/start/", views.StartWorkerView.as_view(), name="start"),
    path("chart/", views.LiveChartView.as_view(), name="livechart"),
    path("error/<int:ex>/", views.NoWorkerAvailableView.as_view(), name="error"),
    path("execution/save/", views.save_analysis_ajax, name="execution-save"),
    path("execution/list/", views.ExecutionListView.as_view(), name="execution-list"),
    path("execution/<int:pk>/detail/", views.ExecutionDetailView.as_view(), name="execution-detail"),
]
