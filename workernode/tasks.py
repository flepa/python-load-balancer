from celery import shared_task, signals, current_app

from utils import mqtt_worker_finder, load_balancer


LB = load_balancer.LoadBalancer()


@shared_task(ignore_result=True)
@signals.worker_ready.connect()
def at_start(sender, **kwargs):
    current_app.send_task("workernode.tasks.find_new_workers")


@shared_task(ignore_result=True, time_limit=7200)
def find_new_workers():
    mqtt_worker_finder.find_new_workers()


@shared_task(ignore_result=True)
def execute_task(worker_node_pk, duration):
    LB.balance_exec(worker_node_pk, duration)


@shared_task(ignore_result=True)
def start_worker(worker_node_pk, update=False, **kwargs):
    LB.start_async_conn(worker_node_pk, update, **kwargs)
