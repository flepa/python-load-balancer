from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView, UpdateView, ListView, DetailView
from django.urls import reverse_lazy

from workernode.forms import TaskForm
from workernode.models import Task, WorkerNode, Execution


class StartWorkerView(UpdateView):
    model = Task
    template_name = "workernode/task_form.html"
    form_class = TaskForm
    success_url = reverse_lazy("workernode:livechart")

    def form_valid(self, form):
        self.object = form.save()

        if available_nodes := WorkerNode.get_available_nodes():
            available_nodes[0].async_launch(self.object)
        else:
            return HttpResponseRedirect(
                reverse_lazy(
                    "workernode:error",
                    kwargs={"ex": 1}
                )
            )
        # return super().form_valid(form)
        return HttpResponseRedirect(self.get_success_url())


class LiveChartView(TemplateView):
    template_name = "workernode/live_chart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["task"] = Task.objects.get(pk=1)

        return context


class NoWorkerAvailableView(TemplateView):
    template_name = "workernode/worker_error.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["ex"] = self.kwargs.get("ex")

        return context


class ExecutionListView(ListView):
    model = Execution
    template_name = "workernode/executions_list.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context["task"] = Task.objects.get(pk=1)

        return context


class ExecutionDetailView(DetailView):
    model = Execution
    template_name = "workernode/execution_detail.html"


@require_POST
@csrf_protect
def save_analysis_ajax(request):
    if request.method == "POST":
        Execution.objects.create(
            json_data=request.POST.get("execution"),
            task_number=request.POST.get("task_number"),
            task_ops=request.POST.get("task_ops"),
            workers_num=request.POST.get("workers_num")
        )

        return JsonResponse({'status': 'OK'})
