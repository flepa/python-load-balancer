# Python Load Balancer

**Python Load Balancer** is a Python application and an **alpha version** of a **distributed system**. A sort of *backbone* for more complex scenarios. The whole system consists in a **centralized authority** (e.g. a Raspberry Pi) which *can distribute* work in a **fair** way among the available **worker nodes** (e.g. ESP32 boards), in the current **network**, thanks to a **load balancer**. The **load balancer** is a **Python algorithm**, the communication between the entities is made through the MQTT and HTTP protocols.

## Scope

The aim is to provide an interaction between users and the **fair load balancer** through a **web application**. With this **web interface**, users can **choose** a task that will be executed on the *available* worker nodes and then see its **execution**, in real time, on the *workers*.

## References

Further information can be found in this [PDF report](python_load_balancer.pdf)

## Note

This project should be combined with [esp32-simple-worker-node](https://gitlab.com/flepa/esp32-simple-worker-node).
