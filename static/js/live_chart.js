const GlobalObject = {
    'registered_workers': [],
};

const defaultChartNumber = 2;

function randomRGB() {
    let o = Math.round, r = Math.random, s = 255;
    return 'rgb(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ', 1)';
}

function applyTransparency(rgbColor) {
    return rgbColor.replace(/[^,]+(?=\))/, '0.5')
}

function onChartReceive(jsonData) {
    let timestamp = new Date(jsonData.timestamp * 1000).toISOString();
    let workerOpsJson = JSON.parse(jsonData.workers_ops);
    // append the new data to the existing chart data
    chart.data.datasets[0].data.push({
        x: timestamp,
        y: jsonData.total_ops,
    });
    chart.data.datasets[1].data.push({
        x: timestamp,
        y: jsonData.target_ops,
    });
    // creating worker specific charts:
    if ((chart.data.datasets.length - defaultChartNumber) < Object.keys(workerOpsJson).length) {
        for (let i = 0; i < Object.keys(workerOpsJson).length; i++) {
            if (! GlobalObject.registered_workers.includes(Object.keys(workerOpsJson)[i])) {
                console.log("adding new dataset!");
                let color = randomRGB();
                GlobalObject.registered_workers.push(Object.keys(workerOpsJson)[i]);
                chart.data.datasets.push({
                    label: 'Op/s by Worker [' + Object.keys(workerOpsJson)[i] + ']',
                    data: [],
                    fill: false,
                    borderColor: color,
                    backgroundColor: applyTransparency(color),
                });
            }
        }
        chart.update();
    }
    chart.data.datasets.forEach(function(dataset, index) {
        if (index >= defaultChartNumber) {
            dataset.data.push({
                x: timestamp,
                y: workerOpsJson[Object.keys(workerOpsJson)[index - defaultChartNumber]],
            });
        }
    });
    // update chart datasets keeping the current animation
    chart.update('quiet');
}

function executionLogger(jsonData) {
    // show on page the number of active workers:
    if (! jsonData.available)
        $('.saturated').show();
    $('#workers_num').text(jsonData.workers);

    if (jsonData.exception) {
        $('.exception-message').text(jsonData.ex_message)
        $('.exception').show();
        if (! jsonData.helper)
            return true;
    }

    return false;
}

function buildChart() {
    let ctx = document.getElementById('myChart').getContext('2d');
    return new Chart(ctx, config);
}

// WEB SOCKET CREATION
const prefix = (window.location.protocol === 'https:') ? 'wss:/' : 'ws://';
const ws_url = prefix + window.location.host + '/ws/livechart/';

const chartSocket = new WebSocket(ws_url);

chartSocket.onopen = function (event) {
    console.log('chartSocket.onopen()', event);
};

chartSocket.onmessage = function (event) {
    console.log('chartSocket.onmessage()', event);
    let data = JSON.parse(event.data);
    // console.log('data', data);
    let failed = executionLogger(data);
    if (! failed)
        onChartReceive(data);
};

chartSocket.onerror = function (e) {
    console.log('chartSocket.onerror()', e);
};

chartSocket.onclose = function (e) {
    console.log('chartSocket.onclose()', e);
};

// CHART CREATION
const config = {
    type: 'line',
    data: {
        datasets: [{
            label: 'Total operations per second',
            data: [],
            fill: false,
            borderColor: 'rgb(241, 162, 8)',
            backgroundColor: 'rgba(241, 162, 8, 0.5)',
        }, {
            label: 'Expected operations per second',
            data: [],
            fill: false,
            borderColor: 'rgb(75, 192, 192)',
            backgroundColor: 'rgba(75, 192, 192, 0.5)',
        }]
    },
    options: {
        legend: {
            display: false
        },
        hover: {
            mode: 'index',
            intersec: false
        },
        scales: {
            y: {
                title: {
                    display: true,
                    text: 'Operations/s'
                },
                min: 0,
                ticks: {
                    stepSize: 50,
                }
            },
            x: {
                type: 'realtime',       // x axis will auto-scroll from right to left
                realtime: {             // per-axis options
                    duration: 35000,    // data in the past 20000 ms will be displayed
                    delay: 2000,        // delay of 2000 ms, so upcoming values are known before plotting a line
                    pause: false,       // chart is not paused
                    ttl: undefined,      // data will be automatically deleted as it disappears off the chart
                }
            }
        },
        plugins: {
            tooltip: {
                mode: 'index',
                intersect: false
            },
            zoom: {
                pan: {
                    enabled: true,    // Enable panning
                    mode: 'x',        // Allow panning in the x direction
                },
            }
        },
    }
};

const chart = buildChart();

setTimeout(() => {
        console.log("TIMEOUT!");
        chart.options.scales['x'].realtime.pause = true;
    },
    33000
);