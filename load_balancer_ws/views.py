from django.views.generic import TemplateView

from workernode.models import Task


class HomepageView(TemplateView):
    """View of the home page."""
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["task"] = Task.objects.get(pk=1)

        return context

